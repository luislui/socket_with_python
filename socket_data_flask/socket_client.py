import socket
import os
import pickle
import re
import threading
import time
cwd = os.getcwd()
import json

import datetime
class Client:
    def __init__(self, target_ip, target_port):
        self.target_ip = target_ip
        self.target_port = target_port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect_to_server()


    def connect_to_server(self):
        try:
            self.s.connect((self.target_ip, int(self.target_port)))
            return True
        except Exception as e:
            raise ConnectionError(e)

    def reconnect(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.target_ip, int(self.target_port)))
    def get_data_from_file(self):
        f2 = open(self.file_name, "r")
        lines = f2.readlines()
        data = []
        for line3 in lines:
            str1_after = re.sub(' +', ' ', line3).strip("\n").split(" ")
            if str1_after:
                data.append(str1_after)
        dict_data = {"data": data, "file_name": self.file_name}
        pick_data = pickle.dumps(dict_data)
        # print(b"||||")
        self.s.sendall(pick_data + b"||||")


    def set_filename(self, file_name):
        file_name = f'{file_name}.txt'
        if not os.path.exists(f"{file_name}"):
            raise FileExistsError("File doesn't exist .")
        else:
            return open(f"{file_name}", "r")

    @property
    def socket_shutdown(self):
        self.s.shutdown(socket.SHUT_RDWR)
    @property
    def socket_close(self):
        self.s.close()
    @property
    def socket_reconnect(self):
        self.s.reconnect()

    def send_frequency_data(self, file_object , send_interval = 1):

            all_line_of_txt = file_object.readlines()
            create_time = "_created_from"+datetime.datetime.now().__str__()
            interval = 0.02
            index = 0
            for line in all_line_of_txt[1:]: # the first line is lable
                # using regex to delete duplicated blank space  and replace the \n to blank space
                clean_text = re.sub(' +', ' ', line).strip("\n").split(" ")
                if clean_text:
                    # structure the sending format
                    dict_data = {"data":clean_text,
                                 "file_name":file_object.name+create_time,
                                 'interval':str(interval),
                                 'index':str(index)}
                    # dict to json (str)
                    dict_data = json.dumps(dict_data)
                    # str to byte
                    send_data = bytes(dict_data,encoding="utf-8")
                    # fill the b'|' into to remain byte to make the length reach 250 byte
                    send_data = send_data.ljust(256,b"|")
                    # send the data
                    print("\n\n now --->")
                    print('sending :',dict_data )
                    self.s.sendall(send_data)
                    interval = round(interval + 0.02,2)
                    index = index + 1
                    # interval
                    time.sleep(send_interval)
            # send 256 b'|' byte to prevent data lost, make sure the server side can get the last one data
            _ = b"".ljust(256,b"|")
            self.s.sendall(_)

target_ip = input("target ip (default 127.0.0.1)：")
if not target_ip:
    target_ip = "127.0.0.1"
target_port = input("target port :")
if not target_port:
    raise ValueError("please input the port")
file_name = input("file name :")
if not file_name:
    raise ValueError("please input the file name")
client = Client(target_ip=target_ip, target_port=target_port)
file = client.set_filename(file_name)
client.send_frequency_data(file,1)