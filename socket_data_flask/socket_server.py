import socket
import threading
import os
import select
import db
import time
import subprocess
import threading
from ast import literal_eval


class Server:
    def __init__(self):
        """
            create TCP socket:
                socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            create UDP socket:
                socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        """
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start_listening(self, port):
        """
            # start accept connections

        :return:
        """
        ip = socket.gethostbyname(socket.gethostname())
        port = port

        self.s.bind((ip, port))
        self.s.listen(100)

        print('Running on IP: ' + ip)
        print('Running on port: ' + str(port))
        # onece you bind the host and start listening , you can start a while loop, to wait for accept client
        while 1:
            c, addr = self.s.accept()
            threading.Thread(target=self.__handle_client, args=(c,)).start()


    def __handle_client(self, c):
            import pickle
            import json
            while True:
                buf_data = b""
                while 1:
                    buf_data +=  c.recv(2)
                    if len(buf_data) > 256:
                        receive_data = buf_data[:256]
                        buf_data = buf_data.replace(receive_data,b"")
                        receive_data = receive_data.replace( b"|", b'')

                        print("---> received" )
                        data = json.loads(receive_data.decode('utf-8'))

                        print(data)
                        i = data['data']
                        a = data['interval']
                        b = data['index']
                        file_name = data["file_name"]
                        db.insert_post_list(a, str(i[0]), i[1], i[2], ax=i[3], ay=i[4], az=i[5], file_name=file_name,
                                                                 data_index=b)




port = int(input('Enter desired port --> '))

server = Server()
server = server.start_listening(port)