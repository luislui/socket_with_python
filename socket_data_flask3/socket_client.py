import socket
import sys
import glob
import os
import time
import pickle
import uuid
# from apscheduler.schedulers.background import BackgroundScheduler
from threading import Timer
import struct
import json
class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer     = None
        self.interval   = interval
        self.function   = function
        self.args       = args
        self.kwargs     = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False

class socket_sender():
    def __init__(self):
        pass
    def send_number(self,filename, data):

        send_data = {"filename": filename + "_" + str(uuid.uuid1()), 'data': data}
        _ = pickle.dumps(send_data)
        num = int_to_bytes(len(_))
        print("send the file size")
        s.send(num)

    def data_retri(self,filename):
        f = open(filename)

        data = []
        a = f.readline()
        while a:
            line = str(a).replace("\n", "")
            line = line.split(" ")
            not_none_list = [i for i in line if i]
            if not_none_list:
                data.append(not_none_list)
            a = f.readline()
        f.close()
        return data[1:]

def int_to_bytes(x: int) -> bytes:
    return x.to_bytes((x.bit_length() + 7) // 8, 'big')


def main(filename,uid):
    sender = socket_sender()
    print("find the file, start sending")
    f = open(filename)

    data = []
    a = f.readline()
    a = f.readline()
    interval  = 0
    index = 1
    while a:
        line = str(a).replace("\n", "")
        line = line.split(" ")
        not_none_list = [i for i in line if i]
        if not_none_list:
            pre_send_data = not_none_list
            dict_data = {"data": pre_send_data, "file_name": f.name +"_"+ uid, 'interval': str(interval),
                         'index': str(index)}
            print("sending...")
            print(dict_data)
            str_data = json.dumps(dict_data)
            byte_data = bytes(str_data, encoding="utf-8")

            #print("len", len(byte_data))
            head_struct = struct.pack('i', len(byte_data))
            #print("len head_struct", len(head_struct))
            #print("len byte_data", len(byte_data))
            s.sendall(head_struct)
            #print("send the head")
            s.sendall(byte_data)
            interval = round(interval + 0.02,2)
            index = index + 1

        a = f.readline()
    # f.close()
    #
    # sender.send_number(filename=filename, data=data)
    # time.sleep(3)
    # while 1:
    #     confirmation = s.recv(1024)
    #     if "you can send data now" in confirmation.decode():
    #         send_data = {"filename": filename + "_" + uid, 'data': data}
    #         _ = pickle.dumps(send_data)
    #         s.sendall(_)
    #         print("receive signal and send the data")
    #         time.sleep(3)
    #         break
    #     time.sleep(1)
    #     print("wait server give me send data signal")
if __name__ == '__main__':
    uid  = str( uuid.uuid1())
    file_list = glob.glob('*.txt')
    host = '127.0.0.1'
    port = 1351
    # file_name = input("")
    s = socket.socket()
    print('[+] Client socket is created.')
    filename = ''
    s.connect((host, port))
    print('[+] Socket is connected to {}'.format(host))
    while filename not in file_list:
        filename = input('file name (make sure it is existed in the same folder with this .py ):')
        filename = filename+'.txt'
    main(filename,uid)
    # confirmation = s.recv(1024)
    # if "listen to update" in confirmation.decode():
    #     print("update scheduler")
    #     sched = BackgroundScheduler()
    #     timer = RepeatedTimer(60, main, filename , uid)





#
# send_data = {"filename": filename+"_"+str(uuid.uuid1()), 'data': data}
# _ = pickle.dumps(send_data)
# num = int_to_bytes(len(_))
# print("send the file size")
# s.send(num)
# confirmation = s.recv(1024)
#
# if "you can send data now" in  confirmation.decode() :
#     print("server got the file size, start send the data now")
#     s.sendall(_)
# import time
#
# time.sleep(3)
#
# confirmation = s.recv(1024)
#
# if "listen to update" in  confirmation.decode() :
#     print("server is listen")
#     send_number()
#     confirmation = s.recv(1024)
#     print("send the number, listern ")
#     if "you can send data now" in confirmation.decode():
#         print("server got the file size, start send the data now")
#         s.sendall(_)
#
