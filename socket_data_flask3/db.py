import psycopg2


class db(object):
    def __init__(self,db,user,pw,host,port):
        self.db = db
        self.user = user
        self.pw = pw
        self.host = host
        self.port = port
    def __enter__(self):
        self.conn = psycopg2.connect(database=self.db,user=self.user,password=self.pw,host=self.host,port=self.port)
        self.cur =self.conn.cursor()
        return self
    def exec(self,exec_query):
        self.cur.execute(exec_query)
        self.conn.commit()
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.cur:
            self.cur.close()
        self.conn.close()

if __name__ == '__main__':
    with db(db='socket_data',user='luilui',pw='1001',host='127.0.0.1',port='5432') as db_instance:
        query = '''
                            insert into data.socket_data (data_timestamp,gz,gx,gy,az,ay,ax,file_name,data_index)
                            VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}');
                            '''.format(1,2,3,4,5,6,7,8,9)
        db_instance.exec(query)

# def insert_post_list(time, gx,gy,gz,az,ax,ay,file_name,data_index):
#     conn = psycopg2.connect(database='socket_data',user='luilui',password='1001',host='127.0.0.1',port='5432')
#     cur = conn.cursor()
#     cur.execute()
# #    rows = cur.fetchall()
#     #print(rows)
#     conn.commit()
#     cur.close()
#     conn.close()
# if __name__ == '__main__':
#     insert_post_list(1,2,3,4,5,6,7)