from flask import Flask , render_template
from flask import request
from flask_cors import CORS
import db_server

app = Flask(__name__, static_url_path='')
CORS(app, resources=r'/*')
import pprint


@app.route('/data')
def data():
    file_name = request.args.get('q')
    _ = db_server.get_data_by_filename(file_name)
    return _

@app.route('/chart')
def chart():
    file_name = request.args.get('q')
    _ = db_server.get_data_by_filename(file_name)
    pprint.pprint(_)
    return render_template("chart.html",data = file_name)
@app.route('/')
def root():
    file_list = db_server.get_all_file()
    _ = []
    for i in file_list:
        _.append(i[0].strip() )
    print(_)
    return render_template("index.html",data = _)



if __name__ == '__main__':
    app.run(port=9000)
