import psycopg2


class postgresql_connector(object):
    def __init__(self,db,user,pw,host,port):
        self.db = db
        self.user = user
        self.pw = pw
        self.host = host
        self.port = port

    def connect(self):
        self.conn = psycopg2.connect(database=self.db,user=self.user,password=self.pw,host=self.host,port=self.port)
        self.cur =self.conn.cursor()

    def exec(self,exec_query):
        self.cur.execute(exec_query)
        self.conn.commit()

    def disconnect(self):
        if self.cur:
            self.cur.close()
        self.conn.close()

if __name__ == '__main__':
    postgresql_connector = postgresql_connector(db='testing_db',user='luilui',pw='1001',host='127.0.0.1',port='5432')
    postgresql_connector.connect()
    query = '''
                               insert into public.testing_tables (data_timestamp,gz,gx,gy,az,ay,ax,file_name,data_index)
                               VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}');
                               '''.format(1, 2, 3, 4, 5, 6, 7, 8, 9)
    postgresql_connector.exec(query)
