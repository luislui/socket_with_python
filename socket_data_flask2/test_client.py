import socket
import struct

HOST = socket.gethostbyname(socket.gethostname())
PORT = 1236

dataBuffer = bytes()
headerSize = 12

sn = 0


def dataHandle(headPack, body):
    global sn
    sn += 1
    print("%s data" % sn)
    print("ver:%s, bodySize:%s, cmd:%s" % headPack)
    print(body.decode())
    print("")


if __name__ == '__main__':
            s= socket.socket()
            s.bind((HOST, PORT))
            s.listen(1)
            conn, addr = s.accept()

            print('Connected by', addr)
            while True:
                data = conn.recv(1024)
                if data:
                    dataBuffer += data
                    while True:
                        if len(dataBuffer) < headerSize:
                            print("data package %s Byte less than" % len(dataBuffer))
                            break

                        headPack = struct.unpack('!3I', dataBuffer[:headerSize])
                        bodySize = headPack[1]
                        _ = len(dataBuffer)
                        # if len(dataBuffer) < headerSize + bodySize:
                        #     print("111 %s Byte222 %s Byte 333" % (len(dataBuffer), headerSize + bodySize))
                        #     break
                        body = dataBuffer[headerSize:headerSize + len(dataBuffer)]

                        dataHandle(headPack, body)

                        dataBuffer = dataBuffer[headerSize + bodySize:]