# Python Version:3.5.1
import socket
import time
import struct
import json

host = "localhost"
port = 1234

ADDR = (host, port)

if __name__ == '__main__':
    client = socket.socket()
    client.connect(ADDR)

    ver = 1
    body = json.dumps(dict(hello="world"))
    print(body)
    cmd = 101
    header = [ver, body.__len__(), cmd]
    headPack = struct.pack("!3I", *header)
    sendData1 = headPack + body.encode()

    ver = 2
    body = json.dumps(dict(hello="world2"))
    print(body)
    cmd = 102
    header = [ver, body.__len__(), cmd]
    headPack = struct.pack("!3I", *header)
    sendData2_1 = headPack + body[:2].encode()
    sendData2_2 = body[2:].encode()
    while 1:
        client.send(sendData2_1)

        client.send(sendData2_2)
        time.sleep(0.02)